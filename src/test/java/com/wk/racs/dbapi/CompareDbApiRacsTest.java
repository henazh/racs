package com.wk.racs.dbapi;

import com.wk.racs.api.config.PropertiesApiRacs;
import com.wk.racs.api.json.customergroup.CustomerGroups;
import com.wk.racs.dbapi.config.BaseTest;
import org.testng.annotations.Test;

public class CompareDbApiRacsTest extends BaseTest {

    private CustomerGroups customerGroupApi;
    private CustomerGroups customerGroupDb;

    @Test
    public void compareDbApiCustomerGroup() throws Exception {
        int idCustomerGroup = PropertiesApiRacs.getInstance().getIdCustomerGroup();
        customerGroupApi = compareDbApiRacsSteps.getCustomerGroupAndCustomersApi(idCustomerGroup);
        customerGroupDb = compareDbApiRacsSteps.getCustomerGroupAndCustomersDb(idCustomerGroup);
        compareDbApiRacsSteps.compareCustomerGroupApiDb(customerGroupApi, customerGroupDb);
        compareDbApiRacsSteps.compareCustomersInsideGroupsApiDb(customerGroupApi, customerGroupDb);
    }

}
