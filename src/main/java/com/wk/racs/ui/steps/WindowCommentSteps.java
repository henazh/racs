package com.wk.racs.ui.steps;

import com.wk.racs.ui.pages.WindowComment;
import com.wk.racs.ui.webdriver.BaseWebDriver;
import org.openqa.selenium.WebDriver;

public class WindowCommentSteps {
    private WebDriver driver;
    private WindowComment windowComment;

    public WindowCommentSteps(WebDriver driver) {
        this.driver = driver;
        windowComment = new WindowComment(driver);
    }

    public void enterCommentAndConfirmationDeleteSelectAllService(String text) {
        windowComment.enterTextareaComment(text);
        windowComment.clickButtonApprove();
        BaseWebDriver.waitDownloadPage(driver);
    }


}
