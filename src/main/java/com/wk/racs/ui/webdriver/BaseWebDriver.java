package com.wk.racs.ui.webdriver;

import com.wk.racs.ui.config.PropertiesUI;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

public class BaseWebDriver {
    private WebDriver driver;
    private static final int TIME_WAIT_BROWSER = 60;
    private static final String NAME_PROGRESSBAR = "//div[@role='progressbar']";

    public BaseWebDriver(WebDriver driver) {
        this.driver = driver;
    }

    public static WebDriver openBrowser() {
        WebDriver driverp;
        System.setProperty("webdriver.chrome.driver", PropertiesUI.getInstance().getPathChromeDriver());
        //WebDriverManager.chromedriver().setup();  // ИЛИ это скачивает последнюю версию автоматически

        ChromeOptions options = new ChromeOptions();
        options.addArguments("--ignore-certificate-errors");                                        // отменяет ошибку неверного сертификата
        options.addArguments("--start-maximized");                                                  // открывает окно браузера сразу максимально
        options.setExperimentalOption("excludeSwitches", new String[]{"enable-automation"}); // убирает надпись в Хроме: Браузером Chrome управляет автоматизированное тестовое ПО
        options.setExperimentalOption("useAutomationExtension", false);

        driverp = new ChromeDriver(options);
        driverp.manage().timeouts().pageLoadTimeout(TIME_WAIT_BROWSER, TimeUnit.SECONDS);
        driverp.manage().timeouts().setScriptTimeout(TIME_WAIT_BROWSER, TimeUnit.SECONDS);
        driverp.manage().timeouts().implicitlyWait(TIME_WAIT_BROWSER, TimeUnit.SECONDS);
        return driverp;
    }

    public static void openPage(WebDriver driver, String url) {
        driver.get(url);
    }

    public static void closeBrowser(WebDriver driver) {
        if (driver != null) { driver.quit(); }
        driver = null;
    }

    public static String getTitlePage(WebDriver driver) {
        return driver.getTitle();
    }

    public static boolean waitWebElementInPage(WebDriver driver, WebElement webElement, int pause) {
        try {
            driver.manage().timeouts().implicitlyWait(pause, TimeUnit.SECONDS);
            WebDriverWait wait = new WebDriverWait(driver, pause);
            wait.until(ExpectedConditions.visibilityOf(webElement)); //.elementToBeClickable(webElement));
            return webElement.isDisplayed();
        } catch (Exception e) {
            return false;
        } finally {
            driver.manage().timeouts().implicitlyWait(TIME_WAIT_BROWSER, TimeUnit.SECONDS);
        }
    }

    public static void waitWebElementInPage(WebDriver driver, WebElement webElement) {
        waitWebElementInPage(driver, webElement, TIME_WAIT_BROWSER);
    }

    public static boolean waitWebElementInPage(WebDriver driver, By by, int pause) {
        try {
            driver.manage().timeouts().implicitlyWait(pause, TimeUnit.SECONDS);
            WebDriverWait wait = new WebDriverWait(driver, pause);
            return wait.until(ExpectedConditions.visibilityOfElementLocated(by)).isDisplayed(); //.elementToBeClickable(webElement));
        } catch (Exception e) {
            return false;
        } finally {
            driver.manage().timeouts().implicitlyWait(TIME_WAIT_BROWSER, TimeUnit.SECONDS);
        }
    }

    public static boolean waitWebElementInPage(WebDriver driver, By by) {
        return waitWebElementInPage(driver, by, TIME_WAIT_BROWSER);
    }

    public static void waitRemoveWebElementInPage(WebDriver driver, By by, int pause) {
        try {
            driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS); // при проверки на отсутсвие всегда 0
            WebDriverWait wait = new WebDriverWait(driver, pause);
            wait.ignoring(StaleElementReferenceException.class, NoSuchElementException.class).until(ExpectedConditions.invisibilityOfElementLocated(by));
        } catch (Exception e) {
        } finally {
            driver.manage().timeouts().implicitlyWait(TIME_WAIT_BROWSER, TimeUnit.SECONDS);
        }
    }

    public static void waitDownloadPage(WebDriver driver) {
        waitWebElementInPage(driver, By.xpath(NAME_PROGRESSBAR), 1);
        waitRemoveWebElementInPage(driver, By.xpath(NAME_PROGRESSBAR), TIME_WAIT_BROWSER);
    }

    public static void realClickWebElement(WebDriver driver, WebElement element) {
        int counter = 1; int poz = 1;
        boolean realClick;
        do {
            waitWebElementInPage(driver, element, 1);
            try {
                counter++;
                element.click();
                realClick = true;
            } catch (Exception e) {
                realClick = false;
            }
        } while ( (counter<TIME_WAIT_BROWSER) && (!realClick) );
    }

    public static void clickUntilSecondElementVisibility(WebDriver driver, WebElement elementFirst, WebElement elementSecond, Boolean waitloadData) {
        int counter = 1;
        boolean realClick;
        waitWebElementInPage(driver, elementFirst);
        do try {
            counter++;
            elementFirst.click();
            realClick = true;
        } catch (Exception e) {
            realClick = false;
        } while ( (counter<TIME_WAIT_BROWSER) && ((!realClick) || (!waitWebElementInPage(driver, elementSecond, 1))) );
        if( waitloadData ){
            waitDownloadPage(driver);
        }
    }

    public static void clickUntilSecondElementVisibility(WebDriver driver, WebElement elementFirst, By elementSecond, Boolean waitloadData) {
        int counter = 1;
        boolean realClick;
        waitWebElementInPage(driver, elementFirst);
        do try {
            counter++;
            elementFirst.click();
            realClick = true;
        } catch (Exception e) {
            realClick = false;
        } while ( (counter<TIME_WAIT_BROWSER) && ((!realClick) || (!waitWebElementInPage(driver, elementSecond, 1))) );
        if( waitloadData ){
            waitDownloadPage(driver);
        }
    }

    public static void clickUntilSecondElementClick(WebDriver driver, WebElement combo, WebElement list, Boolean waitloadData) {
        clickUntilSecondElementVisibility(driver, combo, list, waitloadData);
        realClickWebElement(driver, list);
    }

    public static void clickUntilSecondElementClick(WebDriver driver, WebElement combo, By listPoz, Boolean waitloadData) {
        clickUntilSecondElementVisibility(driver, combo, listPoz, waitloadData);
        realClickWebElement(driver, driver.findElement(listPoz));
    }

    public static void choiceInComboListContainsIdOption(WebDriver driver, WebElement combo, int poz, Boolean waitloadData) {
        clickUntilSecondElementClick(driver, combo, By.xpath( "//div[@role='presentation']//li[contains(@id, '-option-" + poz + "')]"), waitloadData);
    }

    public static void inputText(WebElement element, String text) {
        element.sendKeys(Keys.HOME, Keys.chord(Keys.SHIFT, Keys.END), text);
    }

    public static int getRandomNumberInt(int min, int max) {
        max -= min;
        return (int) (Math.random() * ++max) + min;
    }


}

