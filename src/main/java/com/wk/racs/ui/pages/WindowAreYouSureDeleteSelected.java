package com.wk.racs.ui.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class WindowAreYouSureDeleteSelected {
    private WebDriver driver;

    public WindowAreYouSureDeleteSelected(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    @FindBy(xpath = "//div[@role='dialog']//span[normalize-space(.) = 'Delete']")
    private WebElement confirmationDeleteAllService;


    public void confirmationDeleteAllService() {
        confirmationDeleteAllService.click();
    }
}
