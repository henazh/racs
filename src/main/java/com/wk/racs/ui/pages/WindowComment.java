package com.wk.racs.ui.pages;

import com.wk.racs.ui.webdriver.BaseWebDriver;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class WindowComment {
    private WebDriver driver;

    public WindowComment(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    @FindBy(xpath = "//textarea[@aria-invalid='false' and @rows='5']")
    private WebElement textareaComment;

    @FindBy(xpath = "//button[@type='button']//span[normalize-space(.) = 'Approve']")
    private WebElement buttonApprove;


    public void enterTextareaComment(String text) {
        BaseWebDriver.inputText(textareaComment, text);
    }

    public void clickButtonApprove() {
        buttonApprove.click();
    }

}

