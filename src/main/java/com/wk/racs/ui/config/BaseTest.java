package com.wk.racs.ui.config;

import com.wk.racs.ui.steps.AutorizationSteps;
import com.wk.racs.ui.webdriver.BaseWebDriver;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

public class BaseTest {
    public WebDriver driver;
    public AutorizationSteps authorizationSteps;

    @BeforeSuite
    public void beginAutorizationTest() {
        driver = BaseWebDriver.openBrowser();
        authorizationSteps = new AutorizationSteps(driver);
        // после открытия браузера необходимо открыть страницу, иначе после валидного ввода логина и пароля происходит перенаправление на страницу ввода логина и пароля по 302 ошибки
        authorizationSteps.openPageAutorization();
    }

    @AfterSuite
    public void endAutorizationTest() {
        BaseWebDriver.closeBrowser(driver);
    }

}
