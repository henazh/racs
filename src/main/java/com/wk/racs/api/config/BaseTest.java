package com.wk.racs.api.config;

import io.restassured.RestAssured;

public class BaseTest {

    public void connectRestAssured() {
        RestAssured.baseURI = PropertiesApiRacs.getInstance().getBaseUrlApi();  // https://192.168.11.105:8082/admin/customer/group/26
        RestAssured.port = PropertiesApiRacs.getInstance().getPortApi();
        RestAssured.useRelaxedHTTPSValidation();                                // подменяет сертификат
    }

}
