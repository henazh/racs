package com.wk.racs.api.json.customergroup;

import lombok.Data;

@Data
public class Manager {
    private String id;
    private String name;

}
