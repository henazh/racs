package com.wk.racs.dbapi.config;

import ru.yandex.qatools.properties.PropertyLoader;
import ru.yandex.qatools.properties.annotations.Property;
import ru.yandex.qatools.properties.annotations.Resource;

@Resource.Classpath("dbracs.properties")     // для иницализации класса будет использоваться файл dbracs.poerties
public class PropertiesDbRacs {

    private static volatile PropertiesDbRacs instanse;

    private PropertiesDbRacs() {
        PropertyLoader.populate(this); // инициализация полей класса значениями из файла
    }

    public static PropertiesDbRacs getInstance() {
        if (instanse == null)
            synchronized (PropertiesDbRacs.class) {
                if (instanse == null)
                    instanse = new PropertiesDbRacs();
            }
        return instanse;
    }

    @Property("url.db")
    private String urlDB;

    @Property("db.login")
    private String dbLogin;

    @Property("db.password")
    private String dbPassword;


    public String getUrlDbRacs() {
        return urlDB;
    }

    public String getDbLoginRacs() {
        return dbLogin;
    }

    public String getDbPasswordRacs() {
        return dbPassword;
    }


}
