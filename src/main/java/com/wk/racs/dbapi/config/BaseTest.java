package com.wk.racs.dbapi.config;

import com.wk.racs.api.config.PropertiesApiRacs;
import com.wk.racs.dbapi.steps.CompareDbApiRacsSteps;
import io.restassured.RestAssured;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

import java.sql.Connection;
import java.sql.DriverManager;

public class BaseTest {

    private Connection connectionRacs;
    public CompareDbApiRacsSteps compareDbApiRacsSteps;

    public void connectRestAssured() {
        RestAssured.baseURI = PropertiesApiRacs.getInstance().getBaseUrlApi();  // https://192.168.11.105:8082/admin/customer/group/26
        RestAssured.port = PropertiesApiRacs.getInstance().getPortApi();
        RestAssured.useRelaxedHTTPSValidation();                                // подменяет сертификат
    }

    public Connection connectDbRacs() {
        try {
            Class.forName("org.postgresql.Driver");
            connectionRacs = DriverManager.getConnection
                    (PropertiesDbRacs.getInstance().getUrlDbRacs(), PropertiesDbRacs.getInstance().getDbLoginRacs(), PropertiesDbRacs.getInstance().getDbPasswordRacs());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return connectionRacs;
    }

    @BeforeClass
    public void beginTestCompareDbApiRacs() {
        connectRestAssured();
        connectionRacs = connectDbRacs();
        compareDbApiRacsSteps = new CompareDbApiRacsSteps(connectionRacs);
    }

    @AfterClass
    public void endTestCompareDbApiRacs() throws Exception {
        connectionRacs.close();
    }

}
